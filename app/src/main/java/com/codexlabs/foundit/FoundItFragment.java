package com.codexlabs.foundit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.codexlabs.foundit.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class FoundItFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = "Found It Fragment";

    private TextView mUsername;
    private Button sendButton;
    private Spinner categoriesSpinner;
    private EditText commentsBox;
    private CheckBox openCheckBox;

    //Authentication purpose
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user;

    //Database purpose
    private DatabaseReference mRootRef;
    private DatabaseReference mTicketsRef;
    private DatabaseReference mUsersRef;

    public FoundItFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FoundItFragment newInstance(int sectionNumber) {
        FoundItFragment fragment = new FoundItFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_found_it, container, false);

        ////Define and fulfill the categories spinner {array of choices is at the strings.xml file}
        categoriesSpinner = (Spinner) rootView.findViewById(R.id.categoriesSpinner);
        ArrayAdapter<CharSequence> categoriesAdapter = ArrayAdapter.createFromResource(rootView.getContext(), R.array.categories_array,
                android.R.layout.simple_spinner_dropdown_item);
        categoriesSpinner.setAdapter(categoriesAdapter);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Get components
        mUsername = (TextView) rootView.findViewById(R.id.username);
        sendButton = (Button) rootView.findViewById(R.id.send_ticket_found);
        commentsBox = (EditText) rootView.findViewById(R.id.comment_multiple_line_txt);
        openCheckBox = (CheckBox) rootView.findViewById(R.id.checkBoxOpen);

        mAuth = FirebaseAuth.getInstance();
        Log.d(TAG, "onCreateView");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    updateUI(user);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        return rootView;
    }

    private void updateUI(FirebaseUser user){
        mUsername.setText(user.getDisplayName());
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mTicketsRef = mRootRef.child("tickets");
        mUsersRef = mRootRef.child("users");


        sendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                writeNewTicket();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private Ticket genTicketFromView(){
        Ticket ticket = new Ticket();
        ticket.uid = user.getUid();
        ticket.author = user.getDisplayName();
        ticket.category = categoriesSpinner.getSelectedItem().toString();
        ticket.comments = commentsBox.getText().toString();
        ticket.open = openCheckBox.isChecked();

        return ticket;
    }

    private void writeNewTicket(){
        Log.d(TAG, "SendNewTicket");
        // Create new ticket at /tickets/lost/$ticketid and at
        // /users/$userid simultaneously

        //mRootRef.child(user.getUid()).setValue(genTicketFromView());

        String key = mTicketsRef.push().getKey();
        Ticket ticket = genTicketFromView();
        Map<String, Object> postValues = ticket.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/foundit/"+key, postValues);

//        Map<String, Object> userTicketRef = new HashMap<>();
//        userTicketRef.put(key, "foundit");
//        Map<String, Object> userChildUpdates = new HashMap<>();
//        userChildUpdates.put("/"+user.getUid()+"/tickets/", userTicketRef);

        mTicketsRef.updateChildren(childUpdates);
        mUsersRef.child(user.getUid()).child("tickets").child("foundit").push().setValue(key);
    }
}