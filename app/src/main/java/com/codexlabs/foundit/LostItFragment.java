package com.codexlabs.foundit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.codexlabs.foundit.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class LostItFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = "Lost It Fragment";

    private TextView mUsernameLost;
    private Button sendButtonLost;
    private Spinner categoriesSpinnerLost;
    private EditText commentsBoxLost;
    private CheckBox openCheckBoxLost;

    private FirebaseUser user;

    //Database purpose
    private DatabaseReference mRootRefLost;
    private DatabaseReference mTicketsRefLost;
    private DatabaseReference mUsersRefLost;

    public LostItFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static LostItFragment newInstance(int sectionNumber) {
        LostItFragment fragment = new LostItFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lost_it, container, false);

        ////Define and fulfill the categories spinner {array of choices is at the strings.xml file}
        categoriesSpinnerLost = (Spinner) rootView.findViewById(R.id.categoriesSpinnerl);
        ArrayAdapter<CharSequence> categoriesAdapter = ArrayAdapter.createFromResource(rootView.getContext(), R.array.categories_array,
                android.R.layout.simple_spinner_dropdown_item);
        categoriesSpinnerLost.setAdapter(categoriesAdapter);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mUsernameLost = (TextView) rootView.findViewById(R.id.usernamel);
        sendButtonLost = (Button) rootView.findViewById(R.id.send_ticket_lost);
        commentsBoxLost = (EditText) rootView.findViewById(R.id.comment_multiple_line_txtl);
        openCheckBoxLost = (CheckBox) rootView.findViewById(R.id.checkBoxOpenl);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            updateUI();
        }else{
            Log.d(TAG, "User not logged!");
        }

        return rootView;
    }

    private void updateUI(){
        mUsernameLost.setText(user.getDisplayName());
    }

    @Override
    public void onStart() {
        super.onStart();

        mRootRefLost = FirebaseDatabase.getInstance().getReference();
        mTicketsRefLost = mRootRefLost.child("tickets");
        mUsersRefLost = mRootRefLost.child("users");

        sendButtonLost.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                writeNewTicket();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private Ticket genTicketFromView(){
        Ticket ticket = new Ticket();
        ticket.uid = user.getUid();
        ticket.author = user.getDisplayName();
        ticket.category = categoriesSpinnerLost.getSelectedItem().toString();
        ticket.comments = commentsBoxLost.getText().toString();
        ticket.open = openCheckBoxLost.isChecked();

        return ticket;
    }

    private void writeNewTicket(){
        Log.d(TAG, "writeNewTicket");
        String key = mTicketsRefLost.push().getKey();
        Ticket ticket = genTicketFromView();
        Map<String, Object> postValues = ticket.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/lost/"+key, postValues);

        mTicketsRefLost.updateChildren(childUpdates);
        mUsersRefLost.child(user.getUid()).child("tickets").child("lost").push().setValue(key);
        //Log.d(TAG, ticket.toMap().toString());
        //mRootRefLost.push().setValue("Working");
    }
}