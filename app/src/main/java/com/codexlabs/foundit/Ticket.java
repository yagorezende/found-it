package com.codexlabs.foundit;

/**
 * Created by yagor on 24/05/2017.
 */

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Ticket {

    public String uid;
    public String author;
    public String keywords;
    public String category;
    public double lat;
    public double lon;
    public String date;
    public String picture;
    public String comments;
    public boolean open;

    public Ticket() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
        this("", "", "", "", "", false);
    }

    public Ticket(String uid, String author, String category, String keywords, String comments, boolean open) {
        this.uid = uid;
        this.author = author;
        this.keywords = keywords;
        this.category = category;
        this.lat = 0;
        this.lon = 0;
        this.date = "dd/mm/aaaa";
        this.picture = "base64 pic or path/to/file";
        this.comments = comments;
        this.open = open;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("author", author);
        result.put("category", category);
        result.put("keywords", keywords);
        result.put("lat", lat);
        result.put("lon", lon);
        result.put("date", date);
        result.put("picture", picture);
        result.put("comments", comments);
        result.put("open", open);

        return result;
    }

}
