package com.codexlabs.foundit;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codexlabs.foundit.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Map;

public class AroundFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = "Around It Fragment";

    private DatabaseReference mRootRefAround = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mLostTicketsRef = mRootRefAround.child("tickets").child("lost");
    private DatabaseReference mFoundUsersRef = mRootRefAround.child("tickets").child("foundit");
    private ChildEventListener foundEventListener;

    public AroundFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AroundFragment newInstance(int sectionNumber) {
        AroundFragment fragment = new AroundFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_around, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        Query recentFoundTicketsQuery = mFoundUsersRef.limitToFirst(2);
        Query recentLostTicketsQuery = mFoundUsersRef.limitToFirst(2);
        Log.d(TAG, recentFoundTicketsQuery.toString());
        Log.d(TAG, recentLostTicketsQuery.toString());

        foundEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());
//                Map<String, Object> ticketsValues = dataSnapshot.getValue(Map.class);
//                Log.d(TAG, ticketsValues.get("open").toString());
                // A new comment has been added, add it to the displayed list
                //Map<String, Object> ticketsValues = dataSnapshot.getValue(Map.class);
                //TODO
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
                //TODO
                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so displayed the changed comment.
//                Comment newComment = dataSnapshot.getValue(Comment.class);
//                String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so remove it.
                //String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

                // A comment has changed position, use the key to determine if we are
                // displaying this comment and if so move it.
//                Comment movedComment = dataSnapshot.getValue(Comment.class);
//                String commentKey = dataSnapshot.getKey();

                // ...
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(rootView.getContext(), "Failed to load comments.",
                        Toast.LENGTH_SHORT).show();
            }
        };

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mFoundUsersRef.limitToFirst(2);
        mFoundUsersRef.addChildEventListener(foundEventListener);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mFoundUsersRef != null) {
            mFoundUsersRef.removeEventListener(foundEventListener);
        }
    }
}